#!/bin/bash
odt_name="${1%.md}.odt"
/usr/local/bin/pandoc --from=markdown_mmd --to=odt "$1" -o "$odt_name"
/Applications/LibreOffice.app/Contents/MacOS/soffice --headless --convert-to docx --outdir "$(dirname "$1")" "$odt_name"
rm "$odt_name"
