# Toolchain

Programme, Tools und andere Helferlein, um die Daten in den anderen Repositories zu verwalten.

Im Markdown-Verzeichnis befinden sich Scripte, um Markdown-Dateien in .odt- oder .docx-Dateien zu konvertieren. Im pandoc-Verzeichnis befindet sich dabei die Vorlage, die pandoc zur Generierung benötigt.

